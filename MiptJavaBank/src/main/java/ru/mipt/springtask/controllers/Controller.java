package ru.mipt.springtask.controllers;

import lombok.Data;
import lombok.NonNull;
import org.springframework.web.bind.annotation.*;
import ru.mipt.springtask.models.*;

import javax.validation.constraints.NotNull;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("/bank")
public class Controller {

    ConcurrentHashMap<String, Client> clients = new ConcurrentHashMap<String, Client>();
    ConcurrentHashMap<String, Admin> admins = new ConcurrentHashMap<String, Admin>();
    ConcurrentHashMap<String, Transaction> transactions = new ConcurrentHashMap<String, Transaction>();

    @Data
    private static class CreateClientRequest {
        private Long initial_balance;
    }

    @Data
    private static class TransferRequest {
        String from;
        String to;
        Long sum;
    }

    @Data
    private static class GetBalanceRequest {
        String admin_id;
        String client_id;
    }

    @Data
    private static class CancelRequest {
        private String transaction_id;
    }

    @PostMapping("/create-client")
    public String CreateClient() {
        String id = UUID.randomUUID().toString();
        clients.put(id, new Client(id));
        return id;
    }

    public String CreateClient(@NotNull Long initial_balance) {
        String id = UUID.randomUUID().toString();
        clients.put(id, new Client(id, initial_balance));
        return id;
    }

    @PostMapping("/create-client-with-balance")
    @ResponseBody
    public String CreateClient(@NotNull @RequestBody CreateClientRequest request) {
        return CreateClient(request.getInitial_balance());
    }

    @PostMapping("/create-admin")
    public String CreateAdmin() {
        String id = UUID.randomUUID().toString();
        admins.put(id, new Admin(id));
        return id;
    }

    synchronized String Transfer(String from, String to, Long sum) throws InvalidAmountException, InvalidIdException {
        if (clients.get(to) == null || clients.get(from) == null) {
            throw new InvalidIdException("Wrong user id!");
        }
        Client from_client = clients.get(from);
        Client to_client = clients.get(to);
        String transaction_id = from_client.TransferTo(to_client, sum);
        transactions.put(transaction_id, new Transaction(from, to, sum));
        return transaction_id;
    }

    @PostMapping("/transfer")
    @ResponseBody
    synchronized public String Transfer(@NotNull @RequestBody TransferRequest request) throws InvalidAmountException, InvalidIdException {
        return Transfer(request.getFrom(), request.getTo(), request.getSum());
    }

    synchronized public void Cancel(String transaction_id) throws InvalidIdException, InvalidAmountException {
        Transaction transaction = transactions.get(transaction_id);
        if (transaction == null) {
            throw new InvalidIdException("Wrong transaction id!");
        }
        if (transaction.isCancelled()) {
            return;
        }
        Client receiver = clients.get(transaction.getTo());
        Client sender = clients.get(transaction.getFrom());
        long transaction_sum = transaction.getSum();
        receiver.TransferTo(sender, transaction_sum);
        transaction.Cancel();
    }

    @PostMapping("/cancel")
    @ResponseBody
    synchronized public void Cancel(@NotNull @RequestBody CancelRequest request) throws InvalidAmountException, InvalidIdException {
        Cancel(request.transaction_id);
    }

    @GetMapping("/get-balance-info")
    @ResponseBody
    public Client GetBalanceInfo(@NotNull @RequestBody GetBalanceRequest request) throws InvalidIdException {
        String admin_id = request.getAdmin_id();
        String client_id = request.getClient_id();
        if (!admins.containsKey(admin_id)) {
            throw new InvalidIdException("Wrong admin id!");
        }
        if (!clients.containsKey(client_id)) {
            throw new InvalidIdException("Wrong client id!");
        }
        return clients.get(client_id);
    }

    @GetMapping("/get-all-balances-info")
    @ResponseBody
    public ConcurrentHashMap<String, Client> GetAllBalancesInfo(@NotNull @RequestBody GetBalanceRequest request) throws InvalidIdException {
        String admin_id = request.getAdmin_id();
        if (!admins.containsKey(admin_id)) {
            throw new InvalidIdException("Wrong admin id!");
        }
        return clients;
    }

}
