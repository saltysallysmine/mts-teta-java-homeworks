package ru.mipt.springtask.models;

import lombok.Getter;

public class Admin {

    @Getter
    private final String id;

    public Admin(String id) {
        this.id = id;
    }

}
