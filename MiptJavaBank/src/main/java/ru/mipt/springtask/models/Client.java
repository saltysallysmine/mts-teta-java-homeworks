package ru.mipt.springtask.models;


import lombok.Getter;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class Client {

    @Getter
    private final String id;
    @Getter
    private final AtomicLong balance;

    private void Add(long val) {
        balance.addAndGet(val);
    }

    public Client(String id) {
        this.id = id;
        this.balance = new AtomicLong(0);
    }

    public Client(String id, long initial_balance) {
        this.id = id;
        this.balance = new AtomicLong(initial_balance);
    }

    synchronized public String TransferTo(Client target, long sum) throws InvalidAmountException {
        long bal = balance.longValue();
        if (sum > bal || sum <= 0) {
            throw new InvalidAmountException("Not enough money or negative amount given");
        }
        Add(-sum);
        target.Add(sum);
        return UUID.randomUUID().toString();
    }

}
