package ru.mipt.springtask.models;

public class InvalidAmountException extends Throwable {
    public InvalidAmountException(String errorMessage) {
        super(errorMessage);
    }
}
