package ru.mipt.springtask.models;

public class InvalidIdException extends Exception {
    public InvalidIdException(String errorMessage) {
        super(errorMessage);
    }
}
