package ru.mipt.springtask.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Transaction {
    @Getter
    private final String from, to;
    @Getter
    private final Long sum;
    @Getter
    private volatile boolean cancelled = false;

    public Transaction(String from, String to, Long sum) {
        this.from = from;
        this.to = to;
        this.sum = sum;
    }

    public void Cancel() {
        cancelled = true;
    }
}
