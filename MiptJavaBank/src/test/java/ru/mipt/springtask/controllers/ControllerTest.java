package ru.mipt.springtask.controllers;

import com.google.gson.Gson;
import lombok.Data;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.mipt.springtask.models.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static Controller controller;

    @BeforeAll
    static void InitializeClient() {
        controller = new Controller();
    }

    @Data
    private static class UserDTO {
        private String id;
        private Long balance;

        UserDTO(String id, Long balance) {
            this.setId(id);
            this.setBalance(balance);
        }
    }

    @Data
    private static class GetAllBalancesResponse {
        private Map<String, UserDTO> response;
    }

    private UserDTO RequestBalance(String admin_id, String  client_id) throws Exception {
        Gson gson = new Gson();
        String response = mockMvc.perform(get("/bank/get-balance-info")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"admin_id\":\"%s\", \"client_id\":\"%s\"}".formatted(admin_id, client_id)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        return gson.fromJson(response, UserDTO.class);
    }

    // JUnit tests

    @Test
    void CreateClientTest() {
        // create accounts
        String poor_client_id = controller.CreateClient();
        String wealthy_client_id = controller.CreateClient(100L);
        Client poor_client = controller.clients.get(poor_client_id);
        Client wealthy_client = controller.clients.get(wealthy_client_id);
        // check user id and balance
        assertEquals(poor_client.getId(), poor_client_id);
        assertEquals(poor_client.getBalance().longValue(), 0);
        assertEquals(wealthy_client.getId(), wealthy_client_id);
        assertEquals(wealthy_client.getBalance().longValue(), 100L);
    }

    @Test
    void HttpCreateClientTest() {
        // create accounts
        String poor_client_id = controller.CreateClient();
        String wealthy_client_id = controller.CreateClient(100L);
        Client poor_client = controller.clients.get(poor_client_id);
        Client wealthy_client = controller.clients.get(wealthy_client_id);
        // check user id and balance
        assertEquals(poor_client.getId(), poor_client_id);
        assertEquals(poor_client.getBalance().longValue(), 0);
        assertEquals(wealthy_client.getId(), wealthy_client_id);
        assertEquals(wealthy_client.getBalance().longValue(), 100L);
    }

    @Test
    void TransferTest() {
        // creating clients
        String poor_sender_id = controller.CreateClient();
        String receiver_id = controller.CreateClient();
        String sender_id = controller.CreateClient(100L);
        // get client's objects
        Client poor_sender = controller.clients.get(poor_sender_id);
        Client receiver = controller.clients.get(receiver_id);
        Client sender = controller.clients.get(sender_id);
        // check invalid id handling
        assertThrows(
                InvalidIdException.class,
                () -> controller.Transfer("invalid_id", receiver.getId(), 100L)
        );
        // check invalid amount handling
        assertThrows(
                InvalidAmountException.class,
                () -> controller.Transfer(poor_sender_id, receiver_id, 100L)
        );
        // check approved transfer
        Assertions.assertDoesNotThrow(
                () -> controller.Transfer(sender_id, receiver_id, 50L)
        );
        assertEquals(sender.getBalance().longValue(), 50);
        assertEquals(receiver.getBalance().longValue(), 50);
        // now receiver has 50 coins and sender has 50 coins
        // check that transaction has been added to transaction log
        try {
            String transfer_id = controller.Transfer(sender_id, receiver_id, 50L);
            assertNotNull(controller.transactions.get(transfer_id));
            assertFalse(controller.transactions.get(transfer_id).isCancelled());
        } catch (InvalidAmountException | InvalidIdException exception) {
            return;
        }
        // 2 transactions was approved
        // check transaction log size
        assertEquals(controller.transactions.size(), 2);
    }

    @Test
    void CancelTest() throws InvalidAmountException, InvalidIdException {
        // creating clients
        String receiver_id = controller.CreateClient();
        String sender_id = controller.CreateClient(100L);
        // get client's objects
        Client receiver = controller.clients.get(receiver_id);
        Client sender = controller.clients.get(sender_id);
        // transaction
        String transaction_id = controller.Transfer(sender_id, receiver_id, 50L);
        // cancel transaction
        controller.Cancel(transaction_id);
        assertEquals(sender.getBalance().longValue(), 100L);
        assertEquals(receiver.getBalance().longValue(), 0L);
        Transaction transaction = controller.transactions.get(transaction_id);
        assertTrue(transaction.isCancelled());
    }

    // RestAPI tests

    @Test
    void RestApiCreateClientTest() throws Exception {
        String poor_client_id = mockMvc.perform(post("/bank/create-client"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String wealthy_client_id = mockMvc.perform(post("/bank/create-client-with-balance")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"initial_balance\":100}"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String admin_id = mockMvc.perform(post("/bank/create-admin"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        // Poor client balance
        UserDTO balance_info = RequestBalance(admin_id, poor_client_id);
        assertThat(balance_info.getId(), equalTo(poor_client_id));
        assertThat(balance_info.getBalance(), equalTo(0L));
        // Wealthy client balance
        balance_info = RequestBalance(admin_id, wealthy_client_id);
        assertThat(balance_info.getId(), equalTo(wealthy_client_id));
        assertThat(balance_info.getBalance(), equalTo(100L));
    }

    @Test
    void RestApiTransferTest() throws Exception {
        String poor_client_id = mockMvc.perform(post("/bank/create-client"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String wealthy_client_id = mockMvc.perform(post("/bank/create-client-with-balance")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"initial_balance\":100}"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String admin_id = mockMvc.perform(post("/bank/create-admin"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        // Make transfer
        mockMvc.perform(post("/bank/transfer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"from\": \"%s\", \"to\": \"%s\", \"sum\": 50}"
                                .formatted(wealthy_client_id, poor_client_id)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        // Poor client balance
        UserDTO balance_info = RequestBalance(admin_id, poor_client_id);
        assertThat(balance_info.getBalance(), equalTo(50L));
        // Wealthy client balance
        balance_info = RequestBalance(admin_id, wealthy_client_id);
        assertThat(balance_info.getBalance(), equalTo(50L));
    }

    @Test
    void RestApiCancelTransactionTest() throws Exception {
        String poor_client_id = mockMvc.perform(post("/bank/create-client"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String wealthy_client_id = mockMvc.perform(post("/bank/create-client-with-balance")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"initial_balance\":100}"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String admin_id = mockMvc.perform(post("/bank/create-admin"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        // Make transfer
        String transaction_id = mockMvc.perform(post("/bank/transfer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"from\": \"%s\", \"to\": \"%s\", \"sum\": 50}"
                                .formatted(wealthy_client_id, poor_client_id)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        // Cancel transaction
        mockMvc.perform(post("/bank/cancel")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"transaction_id\": \"%s\"}".formatted(transaction_id)))
                .andExpect(status().isOk());
        // Poor client balance
        UserDTO balance_info = RequestBalance(admin_id, poor_client_id);
        assertThat(balance_info.getBalance(), equalTo(0L));
        // Wealthy client balance
        balance_info = RequestBalance(admin_id, wealthy_client_id);
        assertThat(balance_info.getBalance(), equalTo(100L));
    }

    @Test
    void RestApiGetAllBalancesTest() throws Exception {
        String poor_client_id = mockMvc.perform(post("/bank/create-client"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String wealthy_client_id = mockMvc.perform(post("/bank/create-client-with-balance")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"initial_balance\":100}"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        String admin_id = mockMvc.perform(post("/bank/create-admin"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        // get all balances
        String all_balances = mockMvc.perform(get("/bank/get-all-balances-info")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"admin_id\": \"%s\"}".formatted(admin_id)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        Gson gson = new Gson();
        all_balances = "{\"response\":" + all_balances + "}";
        GetAllBalancesResponse response = gson.fromJson(all_balances, GetAllBalancesResponse.class);
        assertThat(response.getResponse(), allOf(
                hasEntry(poor_client_id, new UserDTO(poor_client_id, 0L)),
                hasEntry(wealthy_client_id, new UserDTO(wealthy_client_id, 100L))
        ));
    }

}