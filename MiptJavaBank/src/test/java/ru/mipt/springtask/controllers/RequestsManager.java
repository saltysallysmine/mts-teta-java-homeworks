//package ru.mipt.springtask.controllers;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.google.gson.Gson;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//
//import java.net.URI;
//import java.io.IOException;
//import java.net.URISyntaxException;
//import java.net.http.HttpClient;
//import java.net.http.HttpRequest;
//import java.net.http.HttpRequest.BodyPublishers;
//import java.net.http.HttpResponse;
//
//public class RequestsManager {
//
//    private static String url;
//
//    RequestsManager(String new_url) {
//        url = new_url;
//    }
//
//    public String CreateClient() throws URISyntaxException, IOException, InterruptedException {
//        HttpRequest http_request = HttpRequest.newBuilder()
//                .uri(new URI(url + "/create-client"))
//                .POST(BodyPublishers.ofString(""))
//                .build();
//        HttpClient http_client = HttpClient.newHttpClient();
//        HttpResponse<String> response = http_client.send(
//                http_request, HttpResponse.BodyHandlers.ofString());
//        return response.body();
//    }
//
//    public String CreateClient(Long initial_balance) throws URISyntaxException, IOException, InterruptedException {
//        ClientDTO client_dto = new ClientDTO();
//        client_dto.setInitial_balance(initial_balance);
//        Gson gson = new Gson();
//        String json_request = gson.toJson(client_dto);
//        HttpRequest http_request = HttpRequest.newBuilder()
//                .uri(new URI(url + "/create-client-with-balance"))
//                .POST(BodyPublishers.ofString(json_request))
//                .header("content_type", "application/json")
//                .build();
//        HttpClient http_client = HttpClient.newHttpClient();
//        HttpResponse<String> response = http_client.send(
//                http_request, HttpResponse.BodyHandlers.ofString());
//        return response.body();
//    }
//
//    public String CreateAdmin() throws URISyntaxException, IOException, InterruptedException {
//        HttpRequest http_request = HttpRequest.newBuilder()
//                .uri(new URI(url + "/create-admin"))
//                .POST(BodyPublishers.ofString(""))
//                .build();
//        HttpClient http_client = HttpClient.newHttpClient();
//        HttpResponse<String> response = http_client.send(
//                http_request, HttpResponse.BodyHandlers.ofString());
//        return response.body();
//    }
//
//    public BalanceDTO GetBalanceInfo(String admin_id, String client_id) throws URISyntaxException, IOException, InterruptedException {
//        HttpRequest http_request = HttpRequest.newBuilder()
//                .uri(new URI(url + "/get-balance-info"))
//                .header("admin_id", admin_id)
//                .header("client_id", client_id)
//                .GET()
//                .build();
//        HttpClient http_client = HttpClient.newHttpClient();
//        HttpResponse<String> response = http_client.send(
//                http_request, HttpResponse.BodyHandlers.ofString());
////        System.out.println(response.body());
//        Gson gson = new Gson();
//        return gson.fromJson(response.body(), BalanceDTO.class);
//    }
//
//    public String DoTransfer(String from, String to, Long amount) throws URISyntaxException, IOException, InterruptedException {
//        TransferDTO transfer_dto = new TransferDTO();
//        transfer_dto.setFrom(from);
//        transfer_dto.setTo(to);
//        transfer_dto.setAmount(amount);
//        Gson gson = new Gson();
//        String json_request = gson.toJson(transfer_dto);
//        HttpRequest http_request = HttpRequest.newBuilder()
//                .uri(new URI(url + "/transfer"))
//                .POST(BodyPublishers.ofString(json_request))
//                .build();
//        HttpClient http_client = HttpClient.newHttpClient();
//        HttpResponse<String> response = http_client.send(
//                http_request, HttpResponse.BodyHandlers.ofString());
//        return response.body();
//    }
//
//}
