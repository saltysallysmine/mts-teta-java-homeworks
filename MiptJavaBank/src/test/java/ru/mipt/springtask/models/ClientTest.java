package ru.mipt.springtask.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.*;

class ClientTest {
    @Test
    void TransferTest() {
        // objects initialization
        String poor_sender_id = "poor_sender_id";
        Client poor_sender = new Client(poor_sender_id);
        String recipient_id = "recipient_id";
        Client recipient = new Client(recipient_id);
        // transfer with empty balance
        assertThrows(InvalidAmountException.class, () -> poor_sender.TransferTo(recipient, 100));
        // transfer with positive balance
        String sender_id = "sender_id";
        Client sender = new Client(sender_id, 100);
        Assertions.assertDoesNotThrow(() -> sender.TransferTo(recipient, 100));
        assertEquals(sender.getBalance().longValue(), new AtomicLong(0).longValue());
        assertEquals(recipient.getBalance().longValue(), new AtomicLong(100).longValue());
    }
}