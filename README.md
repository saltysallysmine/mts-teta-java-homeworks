# Mts Teta Java Homeworks

These projects were originally hosted by me on GitHub, so I'm leaving links to separate projects.

- Bank App. [GitHub](https://github.com/saltysallysmine/mipt-java-bank).
- Backwards Compatibility. [GitHub](https://github.com/saltysallysmine/backwards-compatibility).
- Mipt Java Delivery. [GitHub](https://github.com/saltysallysmine/mipt-java-delivery).
- Transactional Outbox. [GitHub](https://github.com/saltysallysmine/TransactionalOutbox).
- Exactly Once Circuit Breaker. [GitHub](https://github.com/saltysallysmine/ExactlyOnceCircuitBreaker).

